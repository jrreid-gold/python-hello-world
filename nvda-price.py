import yfinance as yf

def show_nvda_price():
    nvda = yf.Ticker("NVDA")
    close_price = nvda.info['regularMarketPreviousClose']
    nvda.basic_info()
    print(nvda.info)
    print(f"The closing price of NVDA yesterday was: {close_price}")

show_nvda_price()

def show_gtlb_price():
    gtlb = yf.Ticker("GTLB")
    close_price = gtlb.info['regularMarketPreviousClose']
    print(f"The closing price of GTLB stock is: {close_price}")

show_gtlb_price()
